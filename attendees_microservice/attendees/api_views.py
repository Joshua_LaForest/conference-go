import email
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AccountVO, Attendee, ConferenceVO
from django.views.decorators.http import require_http_methods
import json
# from events.models import Conference

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "status",
        "href",
        ]

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse({"attendees": attendees}, encoder=AttendeeListEncoder)
    else:
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(attendee,
        encoder=AttendeeshowEncoder,
        safe=False,)


class AttendeeshowEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        ]
    def get_extra_data(self, o):
        account = AccountVO.objects.filter(email = o.email).account()
        if account == 0:
            return {"has_account": True}
        else:
            return {"has_account": False}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendee = Attendee.objects.get(id = pk)
        return JsonResponse({"attendee": attendee}, encoder=AttendeeshowEncoder)
    elif request.method == "DELETE":
        count, _= Attendee.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        try:
            conference = ConferenceVO.objects.get(id = pk)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
            )
        Attendee.objects.filter(id=pk).update(**content)
        attendee = Attendee.objects.get(id = pk)
        return JsonResponse({"attendees": attendee},
        encoder=AttendeeshowEncoder,
        safe=False,)
    # return JsonResponse({
    #     "email": attendees.email,
    #     "name": attendees.name,
    #     "company_name": attendees.company_name,
    #     "created": attendees.created,
    #     "conference": {
    #         "name": attendees.conference.name,
    #         "href": attendees.conference.get_api_url(),
    #     }
    # })
    
    # return JsonResponse({})
