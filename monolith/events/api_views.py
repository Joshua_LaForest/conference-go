
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acls import pic_of_city, get_weather_data


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        # "status",
        # "href",
        ]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse({"conferences": conferences}, encoder = ConferenceListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location= Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(conference,
        encoder=ConferenceshowEncoder,
        safe=False,)
    
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class ConferenceshowEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
        "location",       
        ]
    encoders = {
        "location": LocationListEncoder(),
    }



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, pk):
    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        current_city = conference.location.city
        current_state = conference.location.state.abbreviation
        weather = get_weather_data(
            current_city,
            current_state,
        )
        return JsonResponse({"conference": conference, "weather": weather}, encoder = ConferenceshowEncoder, safe = False)
    elif request.method == "DELETE":
        count, _= Conference.objects.filter(pk = pk).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
            )
        Conference.objects.filter(pk=pk).update(**content)
        conference = Conference.objects.create(**content)
        return JsonResponse(conference,
        encoder=ConferenceshowEncoder,
        safe=False,)
    


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder)
    else:
        content = json.loads(request.body)
        
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
            photo = pic_of_city(content["city"], content["state"].abbreviation)
            content.update(photo)
            print(photo)
        except State.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
            )
        
        location = Location.objects.create(**content)
        return JsonResponse(location,
        encoder=LocationShowEncoder,
        safe=False,)

    


class LocationShowEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "pic_url"
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    if request.method == "GET":
        location = Location.objects.get(id = pk)
        return JsonResponse({"location": location}, encoder=LocationShowEncoder)
    elif request.method == "DELETE":
        count, _= Location.objects.filter(pk = pk).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
            )
        Location.objects.filter(pk=pk).update(**content)
        location = Location.objects.create(**content)
        return JsonResponse(location,
        encoder=LocationShowEncoder,
        safe=False,)
    
    
    
    # return JsonResponse({
    #     "name": location.name,
    #     "city": location.city,
    #     "room_count": location.room_count,
    #     "created": location.created,
    #     "updated": location.updated,
    #     "state": location.state.id,
    # })
