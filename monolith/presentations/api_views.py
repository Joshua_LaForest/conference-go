

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from json import JSONEncoder  #ignore
from common.json import ModelEncoder
from .models import Presentation
import json
from events.models import Conference
# from events.api_views import ConferenceListEncoder
import pika 


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        # "status",
        # "href",
        ]
    
    def get_extra_data(self, o):
        return {"status": o.status.name}

@require_http_methods(["DELETE", "POST", "PUT", "GET"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse({"presentations": presentations}, encoder = PresentationListEncoder)
    # elif request.method == "DELETE":
    #     count, _= Presentation.objects.filter(pk = conference_id).delete()
    #     return JsonResponse({"deleted": count>0})
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            conference= Conference.objects.get(id = conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse({"presentation": presentation},
        encoder=PresentationShowEncoder,
        safe=False,)
    # else:
    #     content = json.loads(request.body)
    #     try:
    #         conference= Conference.objects.get(id = conference_id)
    #         content["conference"] = conference
    #     except Conference.DoesNotExist:
    #         return JsonResponse(
    #         {"message": "Invalid conference id"},
    #         status=400,
    #         )
    #     presentation = Presentation.create(**content)
    #     return JsonResponse({"presentation": presentation},
    #     encoder=PresentationShowEncoder,
    #     safe=False,)
    
    
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})


class PresentationShowEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "synopsis",
        "created",
        "title",
        # "conference",     
        ]
    # encoders = {
    #     "conference": ConferenceListEncoder
    # }
    
    def get_extra_data(self, o):
        return {"status": o.status.name}



@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        presentation = Presentation.objects.get(id = pk)
        return JsonResponse(
            {"presentation": presentation},
            encoder = PresentationShowEncoder
        )
    elif request.method == "DELETE":
        count, _= Presentation.objects.filter(pk = pk).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        try:
            # conference= Conference.objects.get(id = pk)
            # content["conference"] = conference
            if "conference" in content:
                conference = Conference.objects.get(id = content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
            )
        presentation = Presentation.objects.filter(id= pk).update(**content)
        presentation = Presentation.objects.get(id = pk)
        return JsonResponse(presentation,
        encoder=PresentationShowEncoder,
        safe=False,)
    
    
    
    
    # return JsonResponse({
    #     "presenter_name": presentation.presenter_name,
    #     "company_name": presentation.company_name,
    #     "presenter_email": presentation.presenter_email,
    #     "title": presentation.title,
    #     "synopsis": presentation.synopsis,
    #     "created": presentation.created,
    #     "status": presentation.status.id,
    #     "conference": {
    #         "name": presentation.conference.name,
    #         "href": presentation.conference.get_api_url(),
    #     }
    # })






@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id = pk)
    presentation.approve()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="process_approval")
    channel.basic_publish(
        exchange="",
        routing_key="process_approval",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        )
    )
    connection.close()
    return JsonResponse(
        presentation,
        encoder=PresentationShowEncoder, 
        safe = False,
    )

@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id = pk)
    presentation.reject()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="process_rejection")
    channel.basic_publish(
        exchange="",
        routing_key="process_rejection",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        )
    )
    connection.close()
    
    return JsonResponse (
        presentation, 
        encoder=PresentationShowEncoder,
        safe = False
    )