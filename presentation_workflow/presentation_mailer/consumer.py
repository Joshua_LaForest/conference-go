import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import time
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_approval(ch, method, properties, body):
    
            content = json.loads(body)
            send_mail(
            "you have been approved",
            f"{content['presenter_name']} your presentation {content['title']} has been approved",
            "admin@conference.go",
            [content["presenter_email"]],
            fail_silently=False,
            )
            print(" Recieved %r" % body)
        





def process_rejection(ch, method, properties, body):

            content = json.loads(body)
            send_mail(
            "you have been rejected",
            f"{content['presenter_name']} your presentation {content['title']} has been rejected",
            "admin@conference.go",
            [content["presenter_email"]],
            fail_silently=False,
            )
            print(" Recieved %r" % body)
        
            
            
while True:
    try:            
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='process_approval')
        channel.basic_consume(
            queue='process_approval',
            on_message_callback=process_approval,
            auto_ack=True,
            )
        channel.queue_declare(queue='process_rejection')
        channel.basic_consume(
            queue='process_rejection',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
                print("Could not connect to RabbitMQ")
                time.sleep(2.0)